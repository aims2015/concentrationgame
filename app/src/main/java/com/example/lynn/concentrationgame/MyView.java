package com.example.lynn.concentrationgame;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.lynn.concentrationgame.MainActivity.*;

/**
 * Created by lynn on 6/24/2015.
 */

public class MyView extends RelativeLayout {

    public Drawable[] getDrawables(){
        ArrayList<Integer> indices = new ArrayList<>();

        indices.add((int)(drawables.length*Math.random()));

        while (indices.size() < 8) {
            int index = (int)(drawables.length*Math.random());

            while (indices.contains(index))
                index = (int)(drawables.length*Math.random());

            indices.add(index);
        }

        Drawable[] output = new Drawable[indices.size()*2];

        for (int counter=0;counter<output.length/2;counter++) {
            output[counter] = drawables[indices.get(counter)];
            output[counter + output.length/2] = drawables[indices.get(counter)];
        }

        return(output);
    }

    public MyView(Context context) {
        super(context);

        int[] ids = {R.drawable.angrybirdgreen,R.drawable.angrybird,R.drawable.bee,R.drawable.bird2,R.drawable.butterfly,
                     R.drawable.butterfly1,R.drawable.catbrown,R.drawable.catgray,R.drawable.cat,R.drawable.cowhead,R.drawable.dog,
                     R.drawable.dog1,R.drawable.dragon,R.drawable.elephant,R.drawable.elephantporcelaine,R.drawable.firefoxpandared,
                     R.drawable.fishmovie,R.drawable.fish,R.drawable.g12turtle,R.drawable.horse ,R.drawable.ladybird,R.drawable.ladybug,
                     R.drawable.lion,R.drawable.mushroombee,R.drawable.otherbutterfly,R.drawable.panda,R.drawable.rabbit,
                     R.drawable.rabbit1,R.drawable.reindeer,R.drawable.roboticpet,R.drawable.squirrel,R.drawable.turtle,
                     R.drawable.turtle1,R.drawable.turtle2,R.drawable.tweetybird,R.drawable.twitterbirdlanding,R.drawable.welovecats,
                     R.drawable.zebra,R.drawable.zebra1};

        mapOfIds = new HashMap<>();

        drawables = new Drawable[ids.length];

        for (int counter=0;counter<drawables.length;counter++) {
            drawables[counter] = getResources().getDrawable(ids[counter]);

            mapOfIds.put(drawables[counter],counter);
        }

        myImageViews = new MyImageView[16];


        gameView = new GameView(context);

        Drawable[] temp = getDrawables();

        for (int counter=0;counter<myImageViews.length;counter++) {
            myImageViews[counter].setDrawable(temp[counter]);

            int red = (int)(256*Math.random());
            int green = (int)(256*Math.random());
            int blue = (int)(256*Math.random());

            myImageViews[counter].setBackgroundColor(Color.argb(255,red,blue,green));
        }

        addView(gameView);







    }

}
