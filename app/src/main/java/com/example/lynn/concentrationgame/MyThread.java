package com.example.lynn.concentrationgame;

import android.view.View;

import static com.example.lynn.concentrationgame.MainActivity.*;

/**
 * Created by lynn on 6/25/2015.
 */

public class MyThread implements Runnable {
    private MyImageView view;

    public MyThread(MyImageView view) {
        this.view = view;

        new Thread(this).start();
    }

    private void pause(double seconds) {
       try {
           Thread.sleep((int)(seconds*1000));
       } catch (Exception e) {
           System.out.println(e);
       }
    }

    @Override
    public void run() {
       final int idFirst = mapOfIds.get(first.getDrawable());
       final int idSecond = mapOfIds.get(view.getDrawable());

       pause(2);

       first.post(new Runnable(){
           @Override
           public void run() {
               if (idFirst == idSecond) {
                   first.setVisibility(View.INVISIBLE);
                   view.setVisibility(View.INVISIBLE);
               } else {
                   first.removeDrawable();
                   view.removeDrawable();
               }

               first = null;
           }
       });






    }

}
