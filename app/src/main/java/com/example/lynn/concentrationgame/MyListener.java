package com.example.lynn.concentrationgame;

import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.Toast;

import static com.example.lynn.concentrationgame.MainActivity.*;

/**
 * Created by lynn on 6/25/2015.
 */
public class MyListener implements View.OnClickListener {

    @Override
    public void onClick(View v) {
        if (v instanceof MyImageView) {
            MyImageView source = (MyImageView)v;

            source.show();

            if (first == null)
                first = source;
            else {
                new MyThread(source);
            }


        }

    }

}
