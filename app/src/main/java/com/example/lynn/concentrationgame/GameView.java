package com.example.lynn.concentrationgame;

import android.content.Context;
import android.widget.RelativeLayout;

import static com.example.lynn.concentrationgame.MainActivity.*;

/**
 * Created by lynn on 6/25/2015.
 */
public class GameView extends RelativeLayout {

    public GameView(Context context) {
        super(context);

        int[] ids = {R.id.imageview1,R.id.imageview2,R.id.imageview3,R.id.imageview4,
                     R.id.imageview5,R.id.imageview6,R.id.imageview7,R.id.imageview8,
                     R.id.imageview9,R.id.imageview10,R.id.imageview11,R.id.imageview12,
                     R.id.imageview13,R.id.imageview14,R.id.imageview15,R.id.imageview16};

        for (int counter=0;counter<myImageViews.length;counter++) {
           myImageViews[counter] = new MyImageView(context);

           myImageViews[counter].setId(ids[counter]);

           RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(200,200);

           if (counter % 4 != 0)
               layoutParams.addRule(RelativeLayout.RIGHT_OF,ids[counter-1]);

           if (counter / 4 != 0)
               layoutParams.addRule(RelativeLayout.BELOW,ids[counter-4]);

           myImageViews[counter].setLayoutParams(layoutParams);

           myImageViews[counter].setOnClickListener(listener);

           addView(myImageViews[counter]);
        }
    }

}
