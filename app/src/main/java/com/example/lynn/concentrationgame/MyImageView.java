package com.example.lynn.concentrationgame;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Created by lynn on 6/25/2015.
 */
public class MyImageView extends ImageView {
    private Drawable drawable;

    public MyImageView(Context context) {
        super(context);
    }

    public Drawable getDrawable() {
        return(drawable);
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public void show() {
        setImageDrawable(drawable);
    }

    public void removeDrawable() {
        setImageDrawable(null);
    }
}
